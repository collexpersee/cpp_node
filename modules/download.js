/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


const fs = require('fs');
const http = require('http');
const https = require('https');

/**
 * A function to download and save a file from an url.
 * @param {string} url - The url where to download the file
 * @param {string} dest - The path where to save the file
 * @param {function} cb - The callback function to call at the end of the downloading
 */
module.exports = function (url, dest, cb) {
  // init write stream
  const file = fs.createWriteStream(dest);
  let httpMethod;

  // check url
  if (url.indexOf(('https://')) !== -1) httpMethod = https;
  else httpMethod = http;

  // start download
  const request = httpMethod.get(url, (response) => {
    if (response.statusCode !== 200) {
        return cb('Error: can not find rdf file');
    }

    // write file
    response.pipe(file);

    // download is over
    file.on('finish', () => {
        file.close(cb);
    });
  }).on('error', (err) => { // check for request error too
    fs.unlink(dest, (err) => {
        if (err) {
            console.log("failed to delete file")
        }
    });
    cb(err.message);
  });

  // if error we erase the file and print the log
  file.on('error', (err) => {
    fs.unlink(dest, (err) => {
        if (err) {
            console.log("failed to delete file")
        }
    });
    cb(err.message);
  });
};