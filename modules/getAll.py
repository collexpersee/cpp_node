"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import sys
import parser_rdf as parser
import json
import os
import save_picture as picture
import re


if __name__ == "__main__":
    parser = parser.ParserRdf("cache/" + sys.argv[1])
    places = parser.get_all()
    geojson = {
        "type": "FeatureCollection",
        "features": [],
    }
    indice = 0
    for place in places:
        indice += 1
        pic = picture.Picture()
        list_small_pics = pic.get_pictures(indice, place.persee_links, place.bnf_links)
        small_persee_list = list_small_pics[0]
        small_bnf_list = list_small_pics[1]
        dates = re.findall(r"\b\d{1,4}\b", place.history_note)
        if len(dates) == 0:
            first_date = ""
            last_date = ""
        elif len(dates) == 1:
            first_date = int(dates[0])
            last_date = int(dates[0])
        else:
            IntDates = [int(i) for i in dates]
            first_date = min(IntDates)
            last_date = max(IntDates)
        geojson["features"].append({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [float(place.long), float(place.lat)]
            },
            "properties": {
                "name": str(place.name),
                "persee_links": str(place.persee_links),
                "bnf_links": str(place.bnf_links),
                "description": place.description,
                "exactMatches": str(place.various_links),
                "history_note": place.history_note,
                "ancestors": str(place.ancestors),
                "labels": str(place.labels),
                "small_persee_links": str(small_persee_list),
                "small_bnf_links": str(small_bnf_list),
                "first_date": first_date,
                "last_date": last_date,
            }
        })
    # please change here to be sure geojson will be read in the same folder as cpp_server is expecting
    shared_geojson_path = os.getenv("HOME") + '/qgisserverdb/'
    if not os.path.exists(shared_geojson_path):
        os.mkdir(shared_geojson_path)
    with open(shared_geojson_path + sys.argv[1].split(".")[0] +
              ".geojson", 'w') as outfile:
        json.dump(geojson, outfile)
    print("done")
