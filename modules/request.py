"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
import sys
import parser_rdf as parser
import json


if __name__ == "__main__":
    parser = parser.ParserRdf("./cache/" + sys.argv[1])
    places = parser.request(sys.argv[2], sys.argv[3], float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]))
    geojson = {
        "type": "FeatureCollection",
        "features": [],
    }
    for place in places:
        geojson["features"].append({
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [float(place.long), float(place.lat)]
            },
            "properties": {
                "name": str(place.name),
                "persee_links": place.persee_links,
                "bnf_links": place.bnf_links,
                "description": place.description,
                "exactMatches": place.various_links,
                "history_note": place.history_note,
            }
        })
    print(json.dumps(geojson))
