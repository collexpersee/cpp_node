/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


const fs = require('fs');


module.exports = {
    read_theso,
    get_theso_names,
    get_file_by_name,
    date_update
}

/**
 * Read the list of thesauri with the name of the file, the name of the thesaurus, its id, the url to download it and
 * the update date.
 * @returns {Array} theso - The 2-dimensional array that contain all the data
 */
function read_theso() {
    let lines = require('fs').readFileSync("cache/theso_list.txt", 'utf-8')
    .split('\n');
    let thesos = [];
    lines.forEach(function(line) {
        thesos.push(line.split(";"));
    });
    return thesos;
}

/**
 * Get all the names of provided thesauri.
 * @returns {Array} names - List of thesauri names
 */
function get_theso_names() {
    thesos = read_theso();
    names = []
    thesos.forEach(function(theso) {
        names.push(theso[1]);
    });
    return names;
}

/**
 * Get the name of the file for a given thesaurus (with his name).
 * @param {string} name - The thesaurus name
 * @returns {string} file - The filename of the thesaurus
 */
function get_file_by_name(name) {
    thesos = read_theso();
    let file = 'error'
    thesos.forEach(function(theso) {
        if(name === theso[1]) {
            file = theso[0];
        };
    });
    return file;
}

/**
 * Change the last update date for a given thesaurus.
 * @param {string} date - The new update date
 * @param {string} filename - The filename of the thesaurus
 */
function date_update(date, filename) {
    thesos = read_theso();
    let result ="";
    thesos.forEach(function(theso) {
        if(filename === theso[0]) {
            theso[4] = new Date(date).toISOString().slice(0,10);
        };
        result += theso.join(";") + "\n";
    });
    fs.writeFileSync('cache/theso_list.txt', result, 'utf-8');
}