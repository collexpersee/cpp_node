"""
/***************************************************************************
 Pictures
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-04-01
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import requests
from io import BytesIO
import json
from PIL import Image
import ast


class Picture:
    """
       A class used to get pictures via links, and resize them
       ...
       Attributes
       ----------
        path: str
            path to the GeoJson file with links inside
        nameFile: str
            name of the GeoJson file
        typeUrl: table of str
            list of posible url types
    """
    # def __init__(self, path, name_file, type_url):
    #     """
    #        Initiate the picture object
    #        ...
    #        Parameters
    #        ----------
    #         path : str
    #             path to the GeoJson file with links inside
    #         nameFile : str
    #             name of the GeoJson file
    #         typeUrl : table of str
    #             list of posible url types
    #     """
    #     self.path = path
    #     self.name_file = name_file
    #     self.type_url = type_url

    def open_json(path, name_file):
        """
           Get a readable GeoJson
           ...
           Parameters
           ----------
            path : str
                path to the GeoJson file with links inside
            nameFile : str
                name of the GeoJson file
        """

        with open(name_file+path) as json_file:
            file = json.load(json_file)

        return file

    def get_link(path, type_url, name_file):
        """
            Get the lists of links to the pictures
            ...
            Parameters
            ----------
            path : str
                path to the GeoJson file with links inside
            nameFile : str
                name of the GeoJson file
            typeUrl : table of str
                list of posible url types
        """

        file = Picture.open_json(path, name_file)

        list_links = []

        for e in type_url:

            tab_links = []

            for f in file['features']:

                if f['properties'][e] != []:
                    urls = ast.literal_eval(f['properties'][e])

                    for i in urls:
                        tab_links = tab_links + [i]
                else:
                    continue

            list_links = list_links + [tab_links]

        # print (list_links)

        return list_links

    def get_pictures(self, indice, persee_list, bnf_list):
        """
            Access to the pictures and save them on a small format
            ...
            Parameters
            ----------
            indice : int
                path to the GeoJson file with links inside
            persee_list: List[string]
                list with Persee links
            bnf_list : List[string]
                list with BNF links
            Return
            ----------
            a list with :
                small_persee_list : list of path containing small
                pictures of original files given in persee_list
                small_bnf_list : same with bnf_list

        """
        small_persee_links = []
        small_bnf_links = []
        # retrieve position of indice, set at 0 at the beginning
        # path_img is img_persee and img_bnf
        path_img = './cache/img_persee/'
        i = 0
        for elem1 in persee_list:
            if len(elem1.split(".jpeg")) == 1 or len(elem1.split(".jpg")) == 1 or len(elem1.split(".png")) == 1:
                # invalid format
                i += 1
                continue
            else:
                response = requests.get(elem1)
                img = Image.open(BytesIO(response.content))
                width, height = img.size
                # print(height)
                new_width = (150*width)/height
                img_changed = img.resize((int(new_width), 150))
                # save with format : img_persee/indice of place_i.jpeg
                tmp_path = path_img + str(indice) + '_' + str(i)
                img_changed.save(tmp_path, 'jpeg')
                small_persee_links.append(tmp_path)
            i += 1
        # same with bnf_list
        i = 0
        path_img = './cache/img_bnf/'
        for elem1 in bnf_list:
            response = requests.get(elem1)
            img = Image.open(BytesIO(response.content))

            width, height = img.size
            # print(height)
            new_width = (150*width)/height
            img_changed = img.resize((int(new_width), 150))
            # save with format : img_bnf/indice of place_i.jpeg
            tmp_path = path_img + str(indice) + '_' + str(i)
            img_changed.save(tmp_path, 'jpeg')
            small_bnf_links.append(tmp_path)
            i += 1
        print('created small pics : ' + str(len(persee_list)+len(bnf_list)))
        # add the two list for return
        ll = [small_persee_links, small_bnf_links]
        return ll
