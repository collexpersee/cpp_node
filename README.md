# Cpp_node : Collex persée node part

This is one of the three part of the collex persée project. (cf : https://gitlab.com/collexpersee/cpp_server)

This part manages queries from user and the update of the data.

## Expose api on 3000 port

Once the server is started (launch server.js) it can be call by several address:

* **/getThesos** to retrieve the list of thesauri (no option)

* **/getCategories** to retrieve the category tree of a given thesaurus (option: theso=the name of the thesaurus)

* **/getPicture** to retrieve a particular picture (option: name=the name of the picture)

* **/query** to get a geojson of the places (option: theso=the name of the thesaurus, category=category of places to search, keyword=keyword to search in labels,lat=latitude of the buffer center to search around, long=longitude of the buffer center, radius=size of the buffer (km)) 
**Warning:** this query can be very long (2-3 min)

## Data update task

The **daemon.js** code checks every hour the update date of all thesauri, updates and exports them if there is some changes.


> Authors : A. Fondere, C. Crapart, E. Viegas, M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski