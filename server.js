/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


const express = require('express');
const { spawn } = require('child_process');
const path = require('path');
const download = require('./modules/download.js');
const reader = require('./modules/read_theso.js');
const app = express();
const fs = require('fs');

app.listen(3000, function () {
  console.log('server running on port 3000');
})

// the address for making queries
app.get('/query', rdf_query);

/**
 * Callback function to query a thesaurus after a request on the /query address.
 * @param req - The request object
 * @param res - The response object
 */
function rdf_query(req, res) {
    let file = reader.get_file_by_name(decodeURI(req.query.theso));
    if (file === 'error') {
        res.send("error in thesaurus name");
        return;
    }
    var input = [
        "./modules/request.py",
        file, // the name of the thesaurus file
        decodeURI(req.query.category), // category of places to search
        decodeURI(req.query.keyword), // keyword to search
        decodeURI(req.query.lat), //latitude
        decodeURI(req.query.long), // longitude
        decodeURI(req.query.radius) // radius around the point
    ];
    const python = spawn('python3',input);

    python.stdout.on('data', (data) => {
    res.setHeader('Content-Type', 'application/json');
    res.json(JSON.parse(data.toString()));
    });
    python.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
    res.send("error, check parameter, maybe the thesaurus is corrupted");
    });

    python.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    });

}

// the address for adding thesaurus
//app.get('/addTheso', addTheso)

/**
 * Callback function to add a thesaurus after a request on the /addTheso address. Disable because of security.
 * @param req - The request object
 * @param res - The response object
 */
function addTheso(req, res) {
    var url = decodeURI(req.query.link);
    download(url, "./cache/" + url.split('/').pop() + ".xml", (err) => {
        if (err) {
            res.send("Error, check address");
        } else {
            res.send('Downloaded');
        }
    })
}


// the address for getting list of thesaurus
app.get('/getThesos', getThesos)

/**
 * Callback function to get the list of thesaurus after a request on the /getThesos address.
 * @param req - The request object
 * @param res - The response object
 */
function getThesos(req, res) {
    const directoryPath = path.join(__dirname, 'cache');
    //passsing directoryPath and callback function
    let names = reader.get_theso_names();
    res.json(names);
}

// the address for getting list of categories
app.get('/getCategories', getCategories)

/**
 * Callback function to get the list of categories from a thesaurus after a request on the /getCategories address.
 * @param req - The request object
 * @param res - The response object
 */
function getCategories(req, res) {
    let file = reader.get_file_by_name(decodeURI(req.query.theso));
    if (file === 'error') {
        res.send("error in thesaurus name");
        return;
    }
    var input = [
        "./modules/getCategories.py",
        file // the name of the thesaurus file
    ];
    const python = spawn('python3',input);

    python.stdout.on('data', (data) => {
        res.setHeader('Content-Type', 'application/json');
        res.json(JSON.parse(data.toString()));
    });
    python.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
        res.send("error, maybe the thesaurus is corrupted");
    });
    python.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
    });
}
// the address for getting list of categories
app.get('/getPicture', getPicture)

/**
 * Callback function to get the list of categories from a thesaurus after a request on the /getCategories address.
 * @param req - The request object
 * @param res - The response object
 */
function getPicture(req, res) {
    let file = decodeURI(req.query.name);
    console.log('print :' + file);
    var s = fs.createReadStream(file);
    s.on('open', function () {
        res.set('Content-Type', 'image/jpeg');
        s.pipe(res);
    });
    s.on('error', function () {
        res.set('Content-Type', 'text/plain');
        res.status(404).end('Not found');
    });
}