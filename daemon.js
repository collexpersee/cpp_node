/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/


const reader = require('./modules/read_theso.js');
const request = require("request");
const download = require('./modules/download.js');
const PythonShell = require('python-shell');
const { spawn } = require('child_process');

/**
 * Function that is launch every hour to check the refresh of the provided thesaurus,
 * call theso_update and date_update if there is some changes in thesaurus.
 */
function check_refresh_date() {
    let thesos = reader.read_theso();
    thesos.forEach(function(theso) {
        request({
            url: theso[3] + "opentheso/api/info/lastupdate?theso=" + theso[2],
            json: true
        }, function (err, res, body) {
            if (!err && res.statusCode === 202) {
                if(Date.parse(theso[4]) < Date.parse(body.lastApdate)) { // Test the refresh of the thesaurus;
                    theso_update(theso[3] + "opentheso/webresources/rest/skos/concept/all/" + theso[0].split('.')[0],
                    theso[0]);
                    reader.date_update(Date.parse(body.lastApdate),theso[0]);
                }
            }
        });
    });
    setTimeout(check_refresh_date, 1800000);
}

/**
 * Download the thesaurus, only launch if there is an update of the thesaurus.
 * @param {string} url - The url where to download the thesaurus
 * @param {string} filename - The name of the output file
 */
function theso_update(url, filename) {
    download(url, "./cache/" + filename, function () {
        get_geojson(filename);
    });
}

/**
 * Parse the new file and refresh the geojson, to do so call a python script.
 * @param {string} filename - The name of the input file
 */
function get_geojson(filename) {
    var input = [
        "./modules/getAll.py",
        decodeURI(filename) // the name of the thesaurus file
    ];
    const python = spawn('python3',input);

    python.stdout.on('data', (data) => {
    console.log(data.toString());
    });

    python.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
    });

    python.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    });

}

check_refresh_date();
